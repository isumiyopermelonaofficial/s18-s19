// [SECTION] JavaScript Objects

// Objects -> is a collection of related data and/or functionalities. The purpose of an object is not only to store multiple data-sets but also to represent real world objects.

// Syntax: 
/*
	let/const variableName = {
		key/property: value
	}

	Note: Information stored in objects are represented in key:value pairing.
*/

// Example - to describe a real world item or object
let cellphone = {
	name: 'Nokia 3120',
	manufactureDate: '1999',
	price: 1000
}; //'Key' -> is also mostly referred to as a property of an object

console.log(cellphone);
console.log(typeof cellphone);

//console.log(cellphone.name);
// How to store multiple objects,

// You can use an array structure to store them.
let users = [
	{
	name: 'Anna',
	age: '23'
	},
	{
	name: 'Nicole',
	age: '18'
	},
	{
	name: 'Smith',
	age: '42'
	},
	{
	name: 'Pam',
	age: '13'
	},
	{
	name: 'Anderson',
	age: '26'
	}
];

console.log(users);
// now there is an alternative way when displaying values of an object collection
console.table(users);

// Complex examples of JS Objects

// Note: Different data types may be stores in an objects's property creating more complex data structures

//When using JS Objects, you can also insert methods

// Methods -> are useful for creating reusable functions that can perform tasks related to an object
let friend = {
	firstName: 'Joe', //string or numbers
	lastName: 'Smith',
	isSingle: true, //boolean
	email: ['joesmith@gmail.com', 'jsmith@company.com'], //array
	address: { //Objects
		city: 'Austin',
		state: 'Texas',
		country: 'USA'
	},
	// methods
	introduce: function(){
		console.log('Hello meet my new friend');
	},
	advice: function(){
		console.log('Give friend an advice to move on');
	},
	wingman: function(){
		console.log('Hype friend when meeting new people');
	}
};

console.log(friend);
